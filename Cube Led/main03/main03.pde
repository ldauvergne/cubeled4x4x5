const int TABLEAULED [4][4]={{3,7,10,11},{2,6,9,12},{1,5,A1,13},{0,4,A0,8}}; // We define the static array related to the hardware
const int PLANLED [4]={A2,A3,A4,A5}; // We define the planes related to the hardware

/* Define the constant size */
int globalsize=4;
int globalsizered=globalsize-1;
/* Matrix buffer waiting to be shown by interrupt*/
 unsigned int cube[4][4]={0};

/* Set bitmask for array selection */
 unsigned int mask=0x0001;

/* Timer2 reload value, globally available */  
volatile unsigned int tcnt2;  

/* Variables used to go trough the matrix */
volatile unsigned int currentx,currenty,currentplane=0;

/* Toggle HIGH or LOW digital write */  
volatile char toggle = 0;

/* Speed factor of launched effects*/
float spdfactor=1;

/* Main program, effects are launched from here*/
void loop() {
/* Boot message */
//welcome();
//delay (1000);
launcheffects(2000);
}


/* Interrupt routine, draws a layer of the cube at a time*/
ISR(TIMER2_OVF_vect) {  
  
  /* Reload the timer */  
  TCNT2 = tcnt2; 
  
  /*Shutdown the previous layer*/
  if (currentplane>0)
  { 
    digitalWrite(PLANLED[currentplane-1],toggle);
  }
  
  /* Enables all the leds of a specified layer */
  for (currenty=0;currenty<globalsize;currenty++)
  {
    for (currentx=0;currentx<globalsize;currentx++)
     {
      digitalWrite(TABLEAULED[currenty][currentx],  ((cube[currentplane][currenty] & (mask << currentx)) >> currentx) );   
     }
  }
   
  /* Lights the z plan and test if we're at the last one, if yes, start from 0 again*/
  digitalWrite(PLANLED[currentplane],~toggle);  
  if (currentplane++ ==globalsize)
  {currentplane=0;}
}


