/* Sets a certain x y z to a value */

void setLED (int x, int y, int z, int value) 
{
  if (inrange(x,y,z)) // Forbids to power up a led out of matrix bounds
  {
    if (value == HIGH)
    cube[z][y] |= (1 << x);
    else
    cube[z][y] &= ~(1 << x);
  }
}

