
#include <Wire.h>
/* Define LED plane addresses */
const char PLANLED [5]={1,2,4,8,16}; // We define the planes related to the hardware

/* Define the constant size */
char globalsize=4;
char globalsizextended=5;
char globalsizered=globalsize-1;

/* Setup for IR receiver 
int RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
decode_results results;*/

/* Matrix buffer waiting to be shown by interrupt*/
volatile  unsigned char cube[5][4]={0};
volatile  unsigned char cubefinal[5][4]={0};

const int tabX[12]={0,0,0,0,1,2,3,3,3,3,2,1};
const int tabY[12]={0,1,2,3,3,3,3,2,1,0,0,0};

/* Used to rotate cube */
volatile unsigned char cubetemp[5][4]={0};

/* Set bitmask for array selection */
unsigned int mask=0x0001;
 
/* Define interrupts variables */
unsigned char portdtemp=0;

/* Timer2 reload value, globally available */  
volatile unsigned int tcnt2;  

/* Variables used to go trough the matrix */
volatile  unsigned int currentx,currenty,currentplane=0;

/* Toggle HIGH or LOW digital write */  
volatile char toggle = 0;

/* Speed factor of launched effects*/
float spdfactor=1;

/* Main program, effects are launched from here*/
void loop() {
 /*
for (int i=0;i<10;i++){ 
for (int z=0;z<13;z++){  
drawnum (0,0,0,0,i,HIGH);
rotate (z,0);
delay (1000);
clearall(LOW);
}
}*/
drawnum (0,0,0,0,2,HIGH);
/*
setLED (1, 1, 0, HIGH) ;
setLED (1, 1, 1, HIGH) ;
setLED (1, 1, 2, HIGH) ;
setLED (1, 1, 3, HIGH) ;
setLED (1, 1, 4, HIGH) ;*/

delay (1000);


for (int i=0;i<12;i++){
rotatecube(1);
delay (400);
}




clearall(LOW);
delay (1000);

/*
setLED (0, 0, 0, HIGH) ;
setLED (2, 0, 0, HIGH) ;
delay (1000);*/

//delay (1000);
//clearall(LOW);
//delay (6000);


  
   
 
/*setLED (0, 0, 0, HIGH) ;
setLED (3, 3, 3, HIGH) ;
//setLED (3, 3, 4, HIGH) ;
setLED (2, 2, 2, HIGH) ;
setLED (1, 1, 1, HIGH) ;*/
/* Boot message */
// sendvoxels_effect(15, 15,50,200);
//welcome();
//delay (1000);
launcheffects(2000);


        
/*digitalWrite(13,HIGH);
    delay(300);
    digitalWrite(13,LOW);
    delay(300);*/
    
}


/* Interrupt routine, draws a layer of the cube at a time*/
ISR(TIMER2_OVF_vect) {  
  
  /* Reload the timer */  
  TCNT2 = tcnt2;
  
  /* Interrupt content */
  
/* Copy the data from the regular cube to the final cube to solve addressing problems */
for (currenty=0;currenty<globalsize;currenty++)
  {
    for (currentx=0;currentx<globalsize;currentx++)
     {
      setLEDfinal(currentx,currenty,currentplane, ((cube[currentplane][currenty] & (mask << currentx)) >> currentx));   
     }
  }

    selectchip3();
    PORTD=0;
    clock();   
    
    portdtemp=cubefinal[currentplane][0];
    portdtemp=portdtemp<<4;

    portdtemp=portdtemp+(cubefinal[currentplane][1]); 
    PORTD=portdtemp;
    selectchip1();
    clock();
    
    portdtemp=cubefinal[currentplane][2];
    portdtemp=portdtemp<<4;

    portdtemp=portdtemp+(cubefinal[currentplane][3]); 
    PORTD=portdtemp;
    selectchip2();
    clock(); 
    
  selectchip3();
    PORTD=PLANLED[currentplane];
    clock(); 
    
   /* Plane reset */
   currentplane++;
   if (currentplane==globalsizextended)
  {currentplane=0;}
  
  /* IR Management 
    if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    irrecv.resume(); // Receive the next value
  }
  */
}


void selectchip1(){
 digitalWrite(9,LOW);
 digitalWrite(10,LOW);
}

void selectchip2(){
  digitalWrite(9,HIGH);
 digitalWrite(10,LOW);
}

void selectchip3(){
  digitalWrite(9,LOW);
 digitalWrite(10,HIGH);
}

void clock(){
 digitalWrite(8,HIGH);
 digitalWrite(8,LOW);
 
}
