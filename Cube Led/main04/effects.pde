/* Sendvoxel effect */

void sendvoxels_effect (int iterations, int voxelrange,  int d, int wait)
{
  for (int i=0;i<iterations;i++)
  {
    sendvoxels_rand_z(voxelrange,d,wait);
  }
}

/* Send a voxel flying from one side of the cube to the other
 It its at the bottom, send it to the top..*/
void sendvoxel_z (unsigned char x, unsigned char y, unsigned char z, int d)
{
	int i, ii;
	for (i=0; i<globalsize; i++)
	{
		if (z == globalsizered)
		{
			ii = globalsizered-i;
			setLED(x,y,ii+1,LOW);
		} else
		{
			ii = i;
			setLED(x,y,ii-1,LOW);
		}
		setLED(x,y,ii,HIGH);
		delay(d);
	}
}

/* For each coordinate along X and Y, a voxel is set either at level 0 or at level 3
 for n iterations, a random voxel is sent to the opposite side of where it was. */
void sendvoxels_rand_z (int iterations, int d, int wait)
{
	unsigned char loop = 16;
	unsigned char x, y, last_x, last_y, i;

	clearall(LOW);

	// Loop through all the X and Y coordinates
	for (x=0;x<globalsize;x++)
	{
		for (y=0;y<globalsize;y++)
		{
			// Then set a voxel either at the top or at the bottom
			if (rand()%2 == 0)
			{
				setLED(x,y,0,HIGH);
			} else
			{
				setLED(x,y,3,HIGH);
			}
		}
	}

	for (i=0;i<iterations;i++)
	{
		// Pick a random x,y position
		x = rand()%globalsize;
		y = rand()%globalsize;
		// but not the sameone twice in a row
		if (y != last_y && x != last_x)
		{
			// If the voxel at this x,y is at the bottom
			if (getvoxel(x,y,0))
			{
				// send it to the top
				sendvoxel_z(x,y,0,d);
			} else
			{
				// if its at the top, send it to the bottom
				sendvoxel_z(x,y,globalsizered,d);
			}
			delay(wait);
			
			// Remember the last move
			last_y = y;
			last_x = x;
		}
	}

}

/* Randomly fill or empty the cube with n voxels. */
void random_filler_effect (int iterations, int lightingleds, int pixels, int d)
{
  for (int i=0; i<iterations; i++)
  {
  random_filler(lightingleds,pixels,d,1);
  clearall(HIGH);
  delay (d*75);
  random_filler(lightingleds,pixels,d,0);
  clearall(LOW);
  delay (d*50);
  }
}

void random_filler (int iterations, int pixels, int d, int state)
{
	int i;
	int p;
	for (i = 0; i < iterations; i++)
	{
		for (p=0;p<=pixels;p++)
			setLED(rand()%globalsize,rand()%globalsize,rand()%globalsize,state);
			
		delay(d);
	}
        
}

/* Snake plane effect with 3 y lines */
void effectsnakeplane1 (int tempo,int repeat) 
{
tempo /= spdfactor;
int j=0;

    for (int i=0; i<19;i++)
    {
       if ((i==16) && (j==repeat)){break;}
       
       if ((i<=2) || (i>15))
       setyline(0,0,HIGH);
       if (i==17)
       setyline(1,0,HIGH);
       if (i==18)
       {
       setyline(1,0,HIGH);
       setyline(2,0,HIGH);
       }
       
       if (i<16)
       setyline(i%globalsize,i/globalsize,HIGH);
     
       if (i>0 && i<16)
       setyline((i-1)%globalsize,(i-1)/globalsize,HIGH);
       if (i>1 && i<16)
       setyline((i-2)%globalsize,(i-2)/globalsize,HIGH);
       
       
       delay (tempo);
       clearall(LOW);
       
       if (i==18)
       {
         i=globalsizered;
          j++; 
       }
    }

}

/* Rain effect: Random voxels light up at the top layer and falls to the bottom layer.*/
void effect_rain (int iterations)
{
	int i;
	int p;		// Position of the raindrop on Z
	int z;		// cube layer
	int y;		// byte
	
	clearall(0x00);

        /*speed*/
	int speed2;

        /*hold*/
        int hold;
        
        /*delay*/
        int delay_ms;
        
	for (i = 0; i < iterations; i++)
	{
		// Start by setting a random pixel on layer 3.
		setLED(rand()%globalsize,rand()%globalsize,globalsizered,HIGH);
                hold=random(50,250)/spdfactor;
		delay(hold);
		
		// The raindrop has to step down one layer 4 times
		// in order to travel from the top, and exit out the bottom.
		for (p=1;p<globalsize;p++)
		{
			// Shift all the layers one position down,
			for (z=0;z<globalsize;z++)
			{
				for (y=0;y<globalsize;y++)
				{
					cube[z][y] = cube[z+1][y];
				}
			}
			
			// and a blank image on the top layer.
			cube[globalsizered][0] = 0x00;
			cube[globalsizered][1] = 0x00;
			cube[globalsizered][2] = 0x00;
			cube[globalsizered][3] = 0x00;
			
			// Accelerate the raindrop as it falls.
			// (speed/p) will decrease as p increases.
                        speed2=random(25,150)/spdfactor;
			delay(speed2+(speed2/p));
		}
		if (p<globalsizered){
		clearall(0x00);
                delay_ms=((hold/random(25,50))*100)/spdfactor;
		delay(delay_ms);
                }
                else
                clearall(LOW);
                delay(random(16,100)/spdfactor);
	}
}

/* Cube effect, draw a cube and then randomly pickup the next drawing point */

void cubical (int tempo, int repeat)
{
  int x=0;
  int y=0;
  for (int i=0; i < repeat; i++)
  {
  y=random(0,10000)%globalsizered;
  while (x==y)
  {
    y=random(0,10000)%globalsizered;
  }
  x=y;
  
  int j=x;
  //Trace the first part of the cube
  cubical1part(j,x,tempo,HIGH);
  //Trace the second part of the cube
  cubical2part(j,x,tempo,HIGH);
  //Trace the third part of the cube
  cubical3part(j,x,tempo,HIGH);

  delay(tempo*2);
  clearall(LOW);
  }
}

void cubical1part(int j, int x,int tempo,int value)
{
    for (j;j<globalsize;j++)
  {
       setLED(j,x,x,value);
       setLED(x,j,x,value);
       setLED(x,x,j,value);
       delay(tempo);
  }
}

void cubical2part(int j, int x,int tempo,int value)
{
    for (j=x+1;j<globalsize;j++)
  {
       setLED(j,globalsizered,x,value);
       setLED(x,globalsizered,j,value);
       
       setLED(globalsizered,j,x,value);
       setLED(globalsizered,x,j,value);
       
       setLED(j,x,globalsizered,value);
       setLED(x,j,globalsizered,value);
       delay(tempo);
  }
}
void cubical3part(int j, int x,int tempo,int value)
{
    for (j=x+1;j<globalsize;j++)
  {
       setLED(j,globalsizered,globalsizered,value);
       setLED(globalsizered,globalsizered,j,value);
       setLED(globalsizered,j,globalsizered,value);
       delay(tempo);
  }
}

// Snake
void boingboing(int iterations, int delayms, unsigned char mode, unsigned char drawmode)
{
	clearall(LOW);		// Blank the cube

	int x, y, z;		// Current coordinates for the point
	int dx, dy, dz;	// Direction of movement
	int lol, i;		// lol?
	unsigned char crash_x, crash_y, crash_z;

	// Coordinate array for the snake.
	int snake[8][3];
	for (i=0;i<8;i++)
	{
		snake[i][0] = 4;
		snake[i][1] = 4;
		snake[i][2] = 4;
	}
	
	y = rand()%4;
	x = rand()%4;
	z = rand()%4;
	
	dx = 1;
	dy = 1;
	dz = 1;
	
	while(iterations)
	{
		crash_x = 0;
		crash_y = 0;
		crash_z = 0;
	

		// Let's mix things up a little:
		if (rand()%3 == 0)
		{
			// Pick a random axis, and set the speed to a random number.
			lol = rand()%3;
			if (lol == 0)
				dx = rand()%3 - 1;
			
			if (lol == 1)
				dy = rand()%3 - 1;
				
			if (lol == 2)
				dz = rand()%3 - 1;
		}

	
		if (dx == -1 && x == 0)
		{
			crash_x = 0x01;
			if (rand()%3 == 1)
			{
				dx = 1;
			} else
			{
				dx = 0;
			}
		}
		
		if (dy == -1 && y == 0)
		{
			crash_y = 0x01;
			if (rand()%3 == 1)
			{
				dy = 1;
			} else
			{
				dy = 0;
			}
		}
		
		if (dz == -1 && z == 0)
		{
			crash_z = 0x01;
			if (rand()%3 == 1)
			{
				dz = 1;
			} else
			{
				dz = 0;
			}
		}
			
		if (dx == 1 && x == 3)
		{
			crash_x = 0x01;
			if (rand()%3 == 1)
			{
				dx = -1;
			} else
			{
				dx = 0;
			}
		}
		
		if (dy == 1 && y == 3)
		{
			crash_y = 0x01;
			if (rand()%3 == 1)
			{
				dy = -1;
			} else
			{
				dy = 0;
			}
		}
		
		if (dz == 1 && z == 3)
		{
			crash_z = 0x01;
			if (rand()%3 == 1)
			{
				dz = -1;
			} else
			{
				dz = 0;
			}
		}
		
		// mode bit 0 sets crash action enable
		if (mode | 0x01)
		{
			if (crash_x)
			{
				if (dy == 0)
				{
					if (y == 3)
					{
						dy = -1;
					} else if (y == 0)
					{
						dy = +1;
					} else
					{
						if (rand()%2 == 0)
						{
							dy = -1;
						} else
						{
							dy = 1;
						}
					}
				}
				if (dz == 0)
				{
					if (z == 3)
					{
						dz = -1;
					} else if (z == 0)
					{
						dz = 1;
					} else
					{
						if (rand()%2 == 0)
						{
							dz = -1;
						} else
						{
							dz = 1;
						}
					}	
				}
			}
			
			if (crash_y)
			{
				if (dx == 0)
				{
					if (x == 3)
					{
						dx = -1;
					} else if (x == 0)
					{
						dx = 1;
					} else
					{
						if (rand()%2 == 0)
						{
							dx = -1;
						} else
						{
							dx = 1;
						}
					}
				}
				if (dz == 0)
				{
					if (z == 3)
					{
						dz = -1;
					} else if (z == 0)
					{
						dz = 1;
					} else
					{
						if (rand()%2 == 0)
						{
							dz = -1;
						} else
						{
							dz = 1;
						}
					}	
				}
			}
			
			if (crash_z)
			{
				if (dy == 0)
				{
					if (y == 3)
					{
						dy = -1;
					} else if (y == 0)
					{
						dy = 1;
					} else
					{
						if (rand()%2 == 0)
						{
							dy = -1;
						} else
						{
							dy = 1;
						}
					}	
				}
				if (dx == 0)
				{
					if (x == 3)
					{
						dx = -1;
					} else if (x == 0)
					{
						dx = 1;
					} else
					{
						if (rand()%2 == 0)
						{
							dx = -1;
						} else
						{
							dx = 1;
						}
					}	
				}
			}
		}
		
		// mode bit 1 sets corner avoid enable
		if (mode | 0x02)
		{
			if (	// We are in one of 8 corner positions
				(x == 0 && y == 0 && z == 0) ||
				(x == 0 && y == 0 && z == 3) ||
				(x == 0 && y == 3 && z == 0) ||
				(x == 0 && y == 3 && z == 3) ||
				(x == 3 && y == 0 && z == 0) ||
				(x == 3 && y == 0 && z == 3) ||
				(x == 3 && y == 3 && z == 0) ||
				(x == 3 && y == 3 && z == 3) 				
			)
			{
				// At this point, the voxel would bounce
				// back and forth between this corner,
				// and the exact opposite corner
				// We don't want that!
			
				// So we alter the trajectory a bit,
				// to avoid corner stickyness
				lol = rand()%3;
				if (lol == 0)
					dx = 0;
				
				if (lol == 1)
					dy = 0;
					
				if (lol == 2)
					dz = 0;
			}
		}
	
		// Finally, move the voxel.
		x = x + dx;
		y = y + dy;
		z = z + dz;
		
		if (drawmode == 0x01) // show one voxel at time
		{
			setLED(x,y,z,HIGH);
			delay(delayms);
			setLED(x,y,z,LOW);	
		} else if (drawmode == 0x02) // flip the voxel in question
		{
			flpvoxel(x,y,z);
			delay(delayms);
		} if (drawmode == 0x03)
		{
			for (i=7;i>=0;i--)
			{
				snake[i][0] = snake[i-1][0];
				snake[i][1] = snake[i-1][1];
				snake[i][2] = snake[i-1][2];
			}
			snake[0][0] = x;
			snake[0][1] = y;
			snake[0][2] = z;
				
			for (i=0;i<8;i++)
			{
				setLED(snake[i][0],snake[i][1],snake[i][2],HIGH);
			}
			delay(delayms);
			for (i=0;i<8;i++)
			{
				setLED(snake[i][0],snake[i][1],snake[i][2],LOW);
			}
		}
		
	
		iterations--;
	}
}

