
const char PLANLED [5]={1,2,4,8,16}; // We define the planes related to the hardware

/* Define the constant size */
int globalsize=5;
int globalsizered=globalsize-1;

/* Matrix buffer waiting to be shown by interrupt*/
volatile  unsigned int cube[4][4]={0};
volatile  unsigned int cubefinal[4][4]={0};

/* Set bitmask for array selection */
 unsigned int mask=0x0001;
 
/* Define interrupts variables */
unsigned char portdtemp=0;

/* Timer2 reload value, globally available */  
volatile unsigned int tcnt2;  

/* Variables used to go trough the matrix */
volatile  unsigned int currentx,currenty,currentplane=0;

/* Toggle HIGH or LOW digital write */  
volatile char toggle = 0;

/* Speed factor of launched effects*/
float spdfactor=1;

/* Main program, effects are launched from here*/
void loop() {
 // clearall(HIGH);
 
/*setLED (0, 0, 0, HIGH) ;
setLED (3, 3, 3, HIGH) ;
//setLED (3, 3, 4, HIGH) ;
setLED (2, 2, 2, HIGH) ;
setLED (1, 1, 1, HIGH) ;*/
/* Boot message */
// sendvoxels_effect(15, 15,50,200);
welcome();
//delay (1000);
//launcheffects(2000);


        
/*digitalWrite(13,HIGH);
    delay(300);
    digitalWrite(13,LOW);
    delay(300);*/
    
}


/* Interrupt routine, draws a layer of the cube at a time*/
ISR(TIMER2_OVF_vect) {  
  
  /* Reload the timer */  
  TCNT2 = tcnt2;
  
    
  
for (currenty=0;currenty<globalsize;currenty++)
  {
    for (currentx=0;currentx<globalsize;currentx++)
     {
      setLEDfinal(currentx,currenty,currentplane, ((cube[currentplane][currenty] & (mask << currentx)) >> currentx));   
     }
  }

    selectchip3();
    PORTD=0;
    clock();   
    
    portdtemp=cubefinal[currentplane][0];
    portdtemp=portdtemp<<4;

    portdtemp=portdtemp+(cubefinal[currentplane][1]); 
    PORTD=portdtemp;
    selectchip1();
    clock();
    
    portdtemp=cubefinal[currentplane][2];
    portdtemp=portdtemp<<4;

    portdtemp=portdtemp+(cubefinal[currentplane][3]); 
    PORTD=portdtemp;
    selectchip2();
    clock(); 
    
  
  
  selectchip3();
    PORTD=PLANLED[currentplane];
    clock(); 
  
    currentplane++;
   if (currentplane==globalsize)
  {currentplane=0;}
  
  /*Shutdown the previous layer*/
  /*if (currentplane>0)
  { 
    digitalWrite(PLANLED[currentplane-1],toggle);
  }*/
  
  /* Enables all the leds of a specified layer */
  /*for (currenty=0;currenty<globalsize;currenty++)
  {
    for (currentx=0;currentx<globalsize;currentx++)
     {
      digitalWrite(TABLEAULED[currenty][currentx],  ((cube[currentplane][currenty] & (mask << currentx)) >> currentx) );   
     }
  }*/
   
  /* Lights the z plan and test if we're at the last one, if yes, start from 0 again*/
  /*digitalWrite(PLANLED[currentplane],~toggle);  
  if (currentplane++ ==globalsize)
  {currentplane=0;}*/
}


void selectchip1(){
  digitalWrite(9,LOW);
 digitalWrite(10,LOW);
}

void selectchip2(){
  digitalWrite(9,HIGH);
 digitalWrite(10,LOW);
}

void selectchip3(){
  digitalWrite(9,LOW);
 digitalWrite(10,HIGH);
}

void clock(){
 digitalWrite(8,HIGH);
 digitalWrite(8,LOW);
 
}


