
void setxline (int y, int z, int value);
void setyline (int x, int z, int value);
void xline (int y, int z, int value,int mini, int maxi, int tempo);
void yline (int x, int z, int value,int mini, int maxi, int tempo);
void zline (int x, int y, int value,int mini, int maxi, int tempo);
void clearall (int value);
unsigned char getvoxel(int x, int y, int z);
unsigned char inrange(int x, int y, int z);

void flpvoxel(int x, int y, int z);
/* Basic line drawing function */

void setxline (int y, int z, int value) {
    for (int i=0; i < globalsize; i++)
    {
      setLED(i,y,z,value);
    }
}

void setyline (int x, int z, int value) {
    for (int i=0; i < globalsize; i++)
    {
      setLED(x,i,z,value);
    }
}

/* Basic line drawing function with tempo */

void xline (int y, int z, int value,int mini, int maxi, int tempo) {
    for (int i=mini; i <= maxi; i++)
    {
      setLED(i,y,z,value);
      delay(tempo);
    }
}

void yline (int x, int z, int value,int mini, int maxi, int tempo) {
    for (int i=mini; i <= maxi; i++)
    {
      setLED(x,i,z,value);
      delay(tempo);
    }
}

void zline (int x, int y, int value,int mini, int maxi, int tempo) {
    for (int i=mini; i <= maxi; i++)
    {
      setLED(x,y,i,value);
      delay(tempo);
    }
}


/* Function to change all the cube leds to a specified value*/
void clearall (int value)
{
  for (int z=0;z<globalsize;z++){
    for (int y=0;y<globalsize;y++){
        for (int x=0;x<globalsize;x++){
           setLED(x,y,z,value);
        }
    }
  }
}


/* Get the current status of a voxel */

unsigned char getvoxel(int x, int y, int z)
{
	if (inrange(x, y, z))
	{
		if (cube[z][y] & (1 << x))
		{
			return 0x01;
		} else
		{
			return 0x00;
		}
	}
}

unsigned char inrange(int x, int y, int z)
{
	if (x >= 0 && x < globalsize && y >= 0 && y < globalsize && z >= 0 && z < globalsize)
	{
		return 0x01;
	} else
	{
		// One of the coordinates was outside the cube.
		return 0x00;
	}
}

// Flip the state of a voxel.
// If the voxel is 1, its turned into a 0, and vice versa.
void flpvoxel(int x, int y, int z)
{
	if (inrange(x, y, z))
		cube[z][y] ^= (1 << x);
}
