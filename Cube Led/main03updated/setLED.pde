/* Sets a certain x y z to a value */
void setLED (int x, int y, int z, int value) 
{
  if (inrange(x,y,z)) // Forbids to power up a led out of matrix bounds
  {
    if (value == HIGH)
    cube[z][y] |= (1 << x);
    else
    cube[z][y] &= ~(1 << x);
  }
}

void setLEDfinal (int x, int y, int z, int value) 
{
  if (inrange(x,y,z)) // Forbids to power up a led out of matrix bounds
  {
    
     if (x==0 && y==0){
      x=3;
      y=1;
    }
      
     else if (x==1 && y==0){
      x=3;
      y=0;
     }
      
    else if (x==2 && y==0){
      x=2;
      y=3;
      }
       
     else if (x==3 && y==0){
      x=3;
      y=3;
      }
       
     else if (x==0 && y==1){
      x=2;
      y=1;
      }
      
     else if (x==1 && y==1){
      x=2;
      y=0;
      }
       
     else if (x==2 && y==1){
      x=0;
      y=3;
      }
       
     else if (x==3 && y==1){
      x=0;
      y=2;
      }
       
     else if (x==0 && y==2){
      x=1;
      y=1;
      }
       
     else if (x==1 && y==2){
      x=1;
      y=0;
      }
       
     else if (x==2 && y==2){
      x=3;
      y=2;
      }
       
     else if (x==3 && y==2){
      x=1;
      y=2;
      }
       
     else if (x==0 && y==3){
      x=0;
      y=1;
      }
       
     else if (x==1 && y==3){
      x=0;
      y=0;
      }
       
     else if (x==2 && y==3){
      x=2;
      y=2;
      }
       
     else{
      x=1;
      y=3;}
      
    if (value == HIGH)
    cubefinal[z][y] |= (1 << x);
    else
    cubefinal[z][y] &= ~(1 << x);
  }
}

