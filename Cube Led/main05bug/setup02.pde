
#include <avr/interrupt.h>



  

void setup() {    
  
  //initialize the digital pin as an output.
  //PORTA
  pinMode(0, OUTPUT);    
  pinMode(1, OUTPUT);  
  pinMode(2, OUTPUT);  
  pinMode(3, OUTPUT);  
  pinMode(4, OUTPUT);  
  pinMode(5, OUTPUT);  
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);    
  //PORT B
  pinMode(8, OUTPUT);  
  pinMode(9, OUTPUT);  
  pinMode(10, OUTPUT);  
  pinMode(13, OUTPUT);  

  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
 
   /* First disable the timer overflow interrupt while we're configuring */  
  TIMSK1 &= ~(1<<TOIE1);  
  
  /* Configure timer2 in normal mode (pure counting, no PWM etc.) */  
  TCCR1A &= ~((1<<WGM11) | (1<<WGM10));  
  TCCR1B &= ~(1<<WGM12);  
  
  /* Select clock source: internal I/O clock */  
  ASSR &= ~(1<<AS2);  
  
  /* Disable Compare Match A interrupt enable (only want overflow) */  
  TIMSK1 &= ~(1<<OCIE1A);  
  
  /* Now configure the prescaler to CPU clock divided by 128 */  
  TCCR1B |= (1<<CS12)  | (1<<CS10); // Set bits  
  TCCR1B &= ~(1<<CS11);             // Clear bit  
  
  /* We need to calculate a proper value to load the timer counter. 
   * The following loads the value 131 into the Timer 2 counter register 
   * The math behind this is: 
   * (CPU frequency) / (prescaler value) = 125000 Hz = 8us. 
   * (desired period) / 8us = 125. 
   * MAX(uint8) + 1 - 125 = 131; 
   */  
  /* Save value globally for later reload in ISR */  
  tcnt2 = 10 ; 
  
  /* Finally load end enable the timer */  
  TCNT2 = tcnt2;  
  TIMSK1 |= (1<<TOIE1);
  
}



