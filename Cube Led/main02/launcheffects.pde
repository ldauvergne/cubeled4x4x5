void launcheffects (int d)
{
int i=0;
while (i!=1)
{
  /* Snake effect: delay, iterations*/
  effectsnakeplane1(random(70,200),5);
  clearall(LOW);
  delay (d);
  
  /* Rain effect: iterations*/
  effect_rain(25);
  clearall(LOW);
  delay (d);
  
  /* Randomly fill the cube: iterations, number of leds to be lighted, pixels, delay, state*/
  random_filler_effect(5,100,1,50);
  clearall(LOW);
  delay (d);
  
  /* Randomly send a voxel from top to bottom */
  sendvoxels_effect(15, 15,50,200);
  clearall(LOW);
  delay (d);
}
}
