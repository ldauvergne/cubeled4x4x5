/* Sendvoxel effect */

void sendvoxels_effect (int iterations, int voxelrange,  int d, int wait)
{
  for (int i=0;i<iterations;i++)
  {
    sendvoxels_rand_z(voxelrange,d,wait);
  }
}

/* Send a voxel flying from one side of the cube to the other
 It its at the bottom, send it to the top..*/
void sendvoxel_z (unsigned char x, unsigned char y, unsigned char z, int d)
{
	int i, ii;
	for (i=0; i<globalsize; i++)
	{
		if (z == globalsizered)
		{
			ii = globalsizered-i;
			setLED(x,y,ii+1,LOW);
		} else
		{
			ii = i;
			setLED(x,y,ii-1,LOW);
		}
		setLED(x,y,ii,HIGH);
		delay(d);
	}
}

/* For each coordinate along X and Y, a voxel is set either at level 0 or at level 3
 for n iterations, a random voxel is sent to the opposite side of where it was. */
void sendvoxels_rand_z (int iterations, int d, int wait)
{
	unsigned char loop = 16;
	unsigned char x, y, last_x, last_y, i;

	clearall(LOW);

	// Loop through all the X and Y coordinates
	for (x=0;x<globalsize;x++)
	{
		for (y=0;y<globalsize;y++)
		{
			// Then set a voxel either at the top or at the bottom
			if (rand()%2 == 0)
			{
				setLED(x,y,0,HIGH);
			} else
			{
				setLED(x,y,3,HIGH);
			}
		}
	}

	for (i=0;i<iterations;i++)
	{
		// Pick a random x,y position
		x = rand()%globalsize;
		y = rand()%globalsize;
		// but not the sameone twice in a row
		if (y != last_y && x != last_x)
		{
			// If the voxel at this x,y is at the bottom
			if (getvoxel(x,y,0))
			{
				// send it to the top
				sendvoxel_z(x,y,0,d);
			} else
			{
				// if its at the top, send it to the bottom
				sendvoxel_z(x,y,globalsizered,d);
			}
			delay(wait);
			
			// Remember the last move
			last_y = y;
			last_x = x;
		}
	}

}

/* Randomly fill or empty the cube with n voxels. */
void random_filler_effect (int iterations, int lightingleds, int pixels, int d)
{
  for (int i=0; i<iterations; i++)
  {
  random_filler(lightingleds,pixels,d,1);
  clearall(HIGH);
  delay (d*75);
  random_filler(lightingleds,pixels,d,0);
  clearall(LOW);
  delay (d*50);
  }
}

void random_filler (int iterations, int pixels, int d, int state)
{
	int i;
	int p;
	for (i = 0; i < iterations; i++)
	{
		for (p=0;p<=pixels;p++)
			setLED(rand()%globalsize,rand()%globalsize,rand()%globalsize,state);
			
		delay(d);
	}
        
}

/* Snake plane effect with 3 y lines */
void effectsnakeplane1 (int tempo,int repeat) 
{
tempo /= spdfactor;
int j=0;

    for (int i=0; i<19;i++)
    {
       if ((i==16) && (j==repeat)){break;}
       
       if ((i<=2) || (i>15))
       setyline(0,0,HIGH);
       if (i==17)
       setyline(1,0,HIGH);
       if (i==18)
       {
       setyline(1,0,HIGH);
       setyline(2,0,HIGH);
       }
       
       if (i<16)
       setyline(i%globalsize,i/globalsize,HIGH);
     
       if (i>0)
       setyline((i-1)%globalsize,(i-1)/globalsize,HIGH);
       if (i>1)
       setyline((i-2)%globalsize,(i-2)/globalsize,HIGH);
       
       
       delay (tempo);
       clearall(LOW);
       
       if (i==18)
       {
         i=globalsizered;
          j++; 
       }
    }

}

/* Rain effect: Random voxels light up at the top layer and falls to the bottom layer.*/
void effect_rain (int iterations)
{
	int i;
	int p;		// Position of the raindrop on Z
	int z;		// cube layer
	int y;		// byte
	
	clearall(0x00);

        /*speed*/
	int speed2;

        /*hold*/
        int hold;
        
        /*delay*/
        int delay_ms;
        
	for (i = 0; i < iterations; i++)
	{
		// Start by setting a random pixel on layer 3.
		setLED(rand()%globalsize,rand()%globalsize,globalsizered,HIGH);
                hold=random(50,250)/spdfactor;
		delay(hold);
		
		// The raindrop has to step down one layer 4 times
		// in order to travel from the top, and exit out the bottom.
		for (p=1;p<globalsize;p++)
		{
			// Shift all the layers one position down,
			for (z=0;z<globalsize;z++)
			{
				for (y=0;y<globalsize;y++)
				{
					cube[z][y] = cube[z+1][y];
				}
			}
			
			// and a blank image on the top layer.
			cube[globalsizered][0] = 0x00;
			cube[globalsizered][1] = 0x00;
			cube[globalsizered][2] = 0x00;
			cube[globalsizered][3] = 0x00;
			
			// Accelerate the raindrop as it falls.
			// (speed/p) will decrease as p increases.
                        speed2=random(25,150)/spdfactor;
			delay(speed2+(speed2/p));
		}
		if (p<globalsizered){
		clearall(0x00);
                delay_ms=((hold/random(25,50))*100)/spdfactor;
		delay(delay_ms);
                }
                else
                clearall(LOW);
                delay(random(16,100)/spdfactor);
	}
}
