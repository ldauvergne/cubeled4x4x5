/* Basic line drawing function */

void setxline (int y, int z, int value) {
    for (int i=0; i < globalsize; i++)
    {
      setLED(i,y,z,value);
    }
}

void setyline (int x, int z, int value) {
    for (int i=0; i < globalsize; i++)
    {
      setLED(x,i,z,value);
    }
}

/* Function to change all the cube leds to a specified value*/
void clearall (int value)
{
  for (int z=0;z<globalsize;z++){
    for (int y=0;y<globalsize;y++){
        for (int x=0;x<globalsize;x++){
           setLED(x,y,z,value);
        }
    }
  }
}

/* Get the current status of a voxel */

unsigned char getvoxel(int x, int y, int z)
{
	if (inrange(x, y, z))
	{
		if (cube[z][y] & (1 << x))
		{
			return 0x01;
		} else
		{
			return 0x00;
		}
	}
}

unsigned char inrange(int x, int y, int z)
{
	if (x >= 0 && x < globalsize && y >= 0 && y < globalsize && z >= 0 && z < globalsize)
	{
		return 0x01;
	} else
	{
		// One of the coordinates was outside the cube.
		return 0x00;
	}
}

