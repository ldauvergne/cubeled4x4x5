static const int tableauLED [4][4]={{3,7,10,11},{2,6,9,12},{1,5,A1,13},{0,4,A0,8}}; // We define the static array related to the hardware
static const int planLED [4]={A2,A3,A4,A5}; // We define the planes related to the hardware

void setLED (int x, int y, int z, int value) 
{
 digitalWrite(planLED[z], value);   // We set plan to the z value
 digitalWrite(tableauLED[y][x], value);   // We set the xy LED to the defined value
}
