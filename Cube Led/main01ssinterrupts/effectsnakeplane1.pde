 void effectsnakeplane1 (int tempo) // Snake plane effect with 3 y line
{ 
  for (int k = 0; k <4; k++) 
  {
    for (int i = 0; i <4;i++ )
    {
      setyline(i,k,HIGH);
      
      if (i>=1)
      {
        setyline(i-1,k,HIGH);
        if (i>=2)
        {
          setyline(i-2,k,HIGH);
        }
      }
      
      delay (tempo);
      setyline(i,k,LOW);
      if (i>=1)
      {
        setyline(i-1,k,LOW);
        if (i>=2)
        {
          setyline(i-2,k,LOW);
        }
        
      }
    }   
  }
}
